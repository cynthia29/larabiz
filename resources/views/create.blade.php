@extends('layouts.app')

@section('content')

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard <span class="float-right" ><a href="/home" class="btn btn-secondary">Back</a></span></div>


                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="post" action="/listings">
                            @csrf
                            <div class="form-group">

                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">

                                <label for="address">Address</label>
                                <input type="text" class="form-control" name="address" id="address" placeholder="Enter address">

                                <label for="website">Website</label>
                                <input type="text" class="form-control" name="website" id="website" placeholder="Enter website">

                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">

                                <label for="phone">Phone</label>
                                <input type="phone" class="form-control" name="phone" id="phone" placeholder="Enter phone">

                                <label for="bio">Bio</label>
                                <input type="text" class="form-control" name="bio" id="bio" placeholder="Enter bio">

                            </div>
                            <button type="submit" class="btn btn-primary">Submit </button>

                        </form>

                    </div>
                </div>
            </div>
        </div>


@endsection
